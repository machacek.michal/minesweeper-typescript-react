import { RevealState } from "./revealState";

export class FieldState {
    public isMine: boolean = false;
    public revealState: RevealState = RevealState.Hidden;
    public minesAround: number = 0;
    public x: number;
    public y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }
}