export class RevealState {
    public static readonly Hidden: number = 0;
    public static readonly Revealed: number = 1;
    public static readonly MarkedAsMine: number = 2;
}