import { GameParams } from "./gameParams";

export class GameStats {
    public revealedCorrectly: number = 0; // todo ugly visibility of those
    public revealedWrongly: number = 0;
    public markedCorrectly: number = 0;
    public markedWrongly: number = 0;
    private readonly allFields: number;
    private readonly mins: number;

    constructor(gameParams: GameParams) {
        this.allFields = gameParams.width * gameParams.height;
        this.mins = gameParams.mines;
    }

    public isWon(): boolean {
        return this.revealedWrongly === 0 &&
               this.revealedCorrectly === (this.allFields - this.mins) &&
               this.markedCorrectly === this.mins &&
               this.markedWrongly === 0;
    }

    public isLost(): boolean {
        return this.revealedWrongly > 0;
    }

    public totalMarked(): number {
        return this.markedCorrectly + this.markedWrongly;
    }

    public mines(): number {
        return this.mins;
    }

    public allFieldsCount(): number {
        return this.allFields;
    }
}