export interface GameParams {
    width: number;
    height: number;
    mines: number;
}

export const EasyGameParams: GameParams = {
    width: 10,
    height: 10,
    mines: 20
};

export const NormalGameParams: GameParams = {
    width: 20,
    height: 12,
    mines: 50
};

export const ExpertGameParams: GameParams = {
    width: 30,
    height: 16,
    mines: 99
};

export const GuruGameParams: GameParams = {
    width: 60,
    height: 32,
    mines: 200
};

export const GameTypes: { [key: string]: GameParams } = {
    "easy": EasyGameParams,
    "normal": NormalGameParams,
    "expert": ExpertGameParams,
    "guru": GuruGameParams
}