import { FieldState } from "./board/fieldState";
import { RevealState } from "./board/revealState";
import { GameParams } from "./gameParams";
import { GameStats } from "./gameStats";

export class Minesweeper {
    private readonly board: FieldState[][];
    private stats: GameStats;
    private wid: number = 0;
    private hei: number = 0;
    private min: number = 0;


    constructor(gameParams: GameParams) {        
        this.wid = gameParams.width;
        this.hei = gameParams.height;
        this.min = gameParams.mines;

        console.log(`Creating new game ${this.wid} x ${this.hei}, ${this.min} mines`);

        this.stats = new GameStats(gameParams);
        this.board = [];

        for (let x = 0; x < this.width; x++) {
            this.board[x] = [];

            for (let y = 0; y < this.height; y++) {
                this.board[x][y] = new FieldState(x, y);
            }
        }

        this.initMines(this.min);
        this.initNeibrs();
    }

    public getAt(x: number, y: number): FieldState {
        return this.board[x][y];
    }

    public getNeibrs(x: number, y: number): FieldState[] {
        const result: FieldState[] = [];

        if (x > 0 && y > 0) {
            result.push(this.board[x - 1][y - 1]);
        }

        if (x > 0) {
            result.push(this.board[x - 1][y]);
        }

        if (x > 0 && y < this.height - 1) {
            result.push(this.board[x - 1][y + 1]);
        }

        if (y > 0) {
            result.push(this.board[x][y - 1]);
        }

        if (y < this.height - 1) {
            result.push(this.board[x][y + 1]);
        }

        if (x < this.width - 1 && y > 0) {
            result.push(this.board[x + 1][y - 1]);
        }

        if (x < this.width - 1) {
            result.push(this.board[x + 1][y]);
        }

        if (x < this.width - 1 && y < this.height - 1) {
            result.push(this.board[x + 1][y + 1]);
        }

        return result;
    }

    public get width(): number {
        return this.wid;
    }

    public get height(): number {
        return this.hei;
    }

    public get mines(): number {
        return this.min;
    }

    public get gameStats(): GameStats {
        return this.stats;
    }

    public get fields(): FieldState[][] {
        return this.board;
    }

    private gameIsActive(){
        return (!this.stats.isLost()) && (!this.stats.isWon());
    }

    public revealAround(x: number, y: number) {
        if(!this.gameIsActive()){
            return;
        }

        const field = this.getAt(x, y);

        if (field.revealState !== RevealState.Revealed) {
            console.log("Cannot reveal around not-revealed field");
            return;
        }

        const neibs = this.getNeibrs(x, y);

        // tslint:disable-next-line: max-line-length
        const neibhsMarkedAsMine = neibs.reduce((sum, element) => sum += (element.revealState === RevealState.MarkedAsMine ? 1 : 0), 0);

        if (field.minesAround !== neibhsMarkedAsMine) {
            console.log("Not correct amount of mines marked around, cannot reveal around");
            return;
        }

        for (const n of neibs) {
            if (n.revealState === RevealState.Hidden) {
                this.reveal(n.x, n.y);
            }
        }
    }

    public reveal(x: number, y: number) {
        if(!this.gameIsActive()){
            return;
        }

        const field = this.getAt(x, y);

        if (field.revealState !== RevealState.Hidden) {
            console.log("Attempted to reveal non-hidden field");
            return;
        }

        this.revealSingleField(field);

        if (field.minesAround === 0) {
            const neibs = this.getNeibrs(x, y);
            for (const n of neibs) {
                if (n.revealState === RevealState.Hidden) {
                    this.reveal(n.x, n.y);
                }
            }
        }
    }

    public revealAll() {
        for (let x = 0; x < this.wid; x++) {
            for (let y = 0; y < this.hei; y++) {
                this.revealSingleField(this.getAt(x, y));
            }
        }
    }

    public fieldAt(x: number, y: number): FieldState {
        return this.getAt(x, y);
    }    

    public markAsMine(x: number, y: number) {
        if(!this.gameIsActive()){
            return;
        }

        const field = this.getAt(x, y);
        const curentState = field.revealState;

        if (curentState === RevealState.Revealed) {
            return;
        }

        // TODO move (un)marking logic into stats
        if (curentState === RevealState.MarkedAsMine) {
            field.revealState = RevealState.Hidden;
            if (field.isMine) {
                this.stats.markedCorrectly--;
            } else {
                this.stats.markedWrongly--;
            }
        } else {
            field.revealState = RevealState.MarkedAsMine;
            if (field.isMine) {
                this.stats.markedCorrectly++;
            } else {
                this.stats.markedWrongly++;
            }
        }
    }

    // TODO better typing
    public getState() {
        return { ...this.stats,
                 isWon: this.stats.isWon(),
                 isLost: this.stats.isLost(),
                 mines: this.stats.mines,
                 totalMarked: this.stats.totalMarked};
    }

    public autoMark() {
        for (let x = 0; x < this.wid; x++) {
            for (let y = 0; y < this.hei; y++) {
                const cell = this.getAt(x, y);

                if (cell.revealState === RevealState.Revealed && cell.minesAround > 0) {
                    const neibs = this.getNeibrs(x, y);
                    const unrevealedNeibs = neibs.filter((c) => c.revealState !== RevealState.Revealed);

                    if (unrevealedNeibs.length === cell.minesAround) {
                        unrevealedNeibs.map((c) => {
                            if (c.revealState === RevealState.Hidden) {
                                this.markAsMine(c.x, c.y);
                            }
                        });
                    }
                }
            }
        }
    }

    public autoReveal() {
        for (let x = 0; x < this.wid; x++) {
            for (let y = 0; y < this.hei; y++) {
                const cell = this.getAt(x, y);

                if (cell.revealState === RevealState.Revealed && cell.minesAround > 0) {
                    const neibs = this.getNeibrs(x, y);
                    const markedNeibs = neibs.filter((c) => c.revealState === RevealState.MarkedAsMine);

                    if (markedNeibs.length === cell.minesAround) {
                        this.revealAround(x, y);
                    }
                }
            }
        }
    }

    public autoAll() {
        let origRevealed = 0;
        let targetRevealed = 0;
        do {
            origRevealed = this.stats.revealedCorrectly + this.stats.revealedWrongly;
            this.autoMark();
            this.autoReveal();
            targetRevealed = this.stats.revealedCorrectly + this.stats.revealedWrongly;
        } while (origRevealed !== targetRevealed)
    }

    private revealSingleField(field: FieldState) {
        field.revealState = RevealState.Revealed;
        if (!field.isMine) {
            this.stats.revealedCorrectly++;
        } else {
            this.stats.revealedWrongly++;
        }
    }

    private initMines(mines: number): void {
        for (let i: number = 0; i < mines; i++) {
            let x: number;
            let y: number;
            do {
                x = Math.round(Math.random() * (this.wid - 1));
                y = Math.round(Math.random() * (this.hei - 1));
            } while (this.getAt(x, y).isMine);

            this.getAt(x, y).isMine = true;
        }
    }

    private initNeibrs() {
        for (let x = 0; x < this.wid; x++) {
            for (let y = 0; y < this.hei; y++) {
                let minesAround = 0;

                const neibs = this.getNeibrs(x, y);

                // TODO refactor using reduce
                for (const n of neibs) {
                    if (n.isMine) {
                        minesAround++;
                    }
                }

                this.getAt(x, y).minesAround = minesAround;
            }
        }
    }
}