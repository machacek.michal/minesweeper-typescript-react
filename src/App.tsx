import React from 'react';
import MinesweeperComponent from './components/minesweeperComponent';

class App extends React.Component {
  render() {
    return (
        <MinesweeperComponent />
    );
  }
}

export default App;