import { SyntheticEvent } from "react";

interface NewGameButtonsProps {
  handleNewGameClick: (e: SyntheticEvent<HTMLButtonElement>) => void;
}

export const NewGameButtons: React.FC<NewGameButtonsProps> = props => {
  return <div>
  <button name="newgame-easy" className="btn btn-primary" onClick={props.handleNewGameClick}>Easy</button> &nbsp;
  <button name="newgame-normal" className="btn btn-primary" onClick={props.handleNewGameClick}>Normal</button> &nbsp;
  <button name="newgame-expert" className="btn btn-primary" onClick={props.handleNewGameClick}>Expert</button> &nbsp;
</div>;
}