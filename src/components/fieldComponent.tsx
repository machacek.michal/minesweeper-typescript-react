import React, { SyntheticEvent, useCallback } from 'react';

export interface CellProps {
  className: string;
  content: string;
  x: number;
  y: number;
  onClick: any;  
}

export const FieldComponent: React.FC<CellProps> = (props: CellProps) => {
  const handleClick = (e: SyntheticEvent) => { 
    console.log(e);
    props.onClick(props.x, props.y, (e as any).button)
  }
  
  const preventDefault = (e: SyntheticEvent) => { e.preventDefault() }

  return <span className={props.className} onMouseDown={handleClick} onContextMenu={preventDefault}>{props.content}</span>
}