import React, { ReactNode } from "react";
import { FieldState } from '../services/board/fieldState';
import { RevealState } from '../services/board/revealState';
import { FieldComponent, CellProps } from './fieldComponent';

interface BoardProps {
  width: number,
  height: number;
  cells: FieldState[][];
  onClick: any;
}

export const BoardComponent: React.FC<BoardProps> = (props: BoardProps) => {
  
  const rows = [];

  for(let y = 0; y<props.height; y++){
    const cols: ReactNode = [];

    for(let x = 0; x<props.width; x++){
      const cellAt = props.cells[x][y];
      const fieldSymbol = mapStateSymbol(cellAt);
      const fieldClass = mapStateClass(cellAt);
      const key = `r${y}c${x}`;

      rows.push(<FieldComponent key={key} className={mapStateClass(cellAt)} content={fieldSymbol} x={x} y={y} onClick={props.onClick}></FieldComponent>)
    }

    rows.push(<div className="row" key={y}>{cols}</div>)
  }
  return <div className="board">{rows}</div>
}

function mapStateSymbol(state: FieldState): string {
  switch (state.revealState) {
      case RevealState.Hidden:
          return "";
      case RevealState.MarkedAsMine:
          return "\u2691";
      case RevealState.Revealed:
          if (state.isMine) {
              return "\uD83D\uDCA3";
          } else {
              return state.minesAround > 0 ? state.minesAround.toString() : "";
          }
      default:
          throw new Error(`Unknown cell state: ${state}`);
  }
}

function mapStateClass(state: FieldState): string {
  switch (state.revealState) {
      case RevealState.Hidden:
          return "field hidden";
      case RevealState.MarkedAsMine:
          return "field marked";
      case RevealState.Revealed:
          return state.isMine ? "field revealedMine" : "field revealed" + state.minesAround.toString();
      default:
          throw new Error(`Unknown cell state: ${state}`);
  }
}