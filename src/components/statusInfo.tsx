import { GameStats } from "../services/gameStats";

interface StatusInfoProps {
  stats: GameStats;
}

export const StatusInfo: React.FC<StatusInfoProps> = props => {
  let statusString = "";

  if (props.stats.isLost()) {
      statusString = "You lost !";
  } else if (props.stats.isWon()) {
      statusString = "You Won !";
  }

  const resolved = (props.stats.revealedCorrectly + props.stats.revealedWrongly + props.stats.markedCorrectly + props.stats.markedWrongly);
  const percent = 100 * resolved / (props.stats.allFieldsCount());
  const percentString = (Math.round(percent * 10) / 10).toFixed(1);
  const status = statusString === "" ? "" : <b> {statusString}</b>;

  return <div>
    Marked <b>{props.stats.totalMarked()}</b> of <b>{props.stats.mines()}</b> mines (<b>{percentString}</b>% of fields revealed){status} 
  </div>
}