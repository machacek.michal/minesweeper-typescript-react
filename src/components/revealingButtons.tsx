import { SyntheticEvent } from "react";

interface RevealingButtonsProps {
  handleRevealButtonsClick: (name: string) => void;
}

export const RevealingButtons: React.FC<RevealingButtonsProps> = props => {
  const clickHandler = (e: SyntheticEvent<HTMLButtonElement>) => {
    const name = (e.target as HTMLButtonElement).name;
    props.handleRevealButtonsClick(name);
  };

  return <div>
      <button name="markAll" className="btn btn-secondary" onClick={clickHandler}>Mark all</button> &nbsp;
      <button name="revealAll" className="btn btn-secondary" onClick={clickHandler}>Reveal all</button>
    </div>;
}