import React, { SyntheticEvent } from 'react';
import { BoardComponent } from './boardComponent';
import { Minesweeper } from '../services/minesweeper';
import { FieldState } from '../services/board/fieldState';
import { EasyGameParams, GameTypes } from '../services/gameParams';
import { NewGameButtons } from './newGameButtons';
import { StatusInfo } from './statusInfo';
import { GameStats } from '../services/gameStats';
import { RevealingButtons } from './revealingButtons';

interface MinesweeperState {
  width: number;
  height: number;
  cells: FieldState[][];
  stats: GameStats;
}

class MinesweeperComponent extends React.Component<{}, MinesweeperState> {
  private mw: Minesweeper;

  constructor(props: any) {
    super(props);
    
    this.mw = new Minesweeper(EasyGameParams);

    this.state = {
      width: this.mw.width,
      height: this.mw.height,
      cells: this.mw.fields,
      stats: this.mw.gameStats
    };

    this.handleCellClick = this.handleCellClick.bind(this);
    this.handleNewGameClick = this.handleNewGameClick.bind(this);
    this.handleRevealingButtonClick = this.handleRevealingButtonClick.bind(this);
  }


  handleCellClick(x: number, y: number, button: number) {
    switch(button){
      case 0: // left
        this.mw.reveal(x,y);
        this.setState({ fields: this.mw.fields, stats: this.mw.gameStats} as unknown as Pick<MinesweeperState, keyof(MinesweeperState)>);
        break;
      case 1: // mid
        this.mw.revealAround(x,y);
        this.setState({ fields: this.mw.fields, stats: this.mw.gameStats} as unknown as Pick<MinesweeperState, keyof(MinesweeperState)>);  
        break;
      case 2: // right
        this.mw.markAsMine(x,y);
        this.setState({ fields: this.mw.fields, stats: this.mw.gameStats} as unknown as Pick<MinesweeperState, keyof(MinesweeperState)>);
        break;
      default:
        console.error("Unexpected button clicked: " + button);
        break;
    }
  }

  handleNewGameClick(e: SyntheticEvent<HTMLButtonElement>){
    const source = (e.target as HTMLButtonElement).name;
    const gameType = source.split("-")[1];
    const game = GameTypes[gameType] || EasyGameParams;

    this.mw = new Minesweeper(game);
    
    this.setState({ 
      cells: this.mw.fields,
      width: this.mw.width,
      height: this.mw.height,
      stats: this.mw.gameStats
    });
  }

  handleRevealingButtonClick(name: string){
    switch(name){
      case "markAll":
        this.mw.autoMark();
        this.setState({ 
          cells: this.mw.fields,
          stats: this.mw.gameStats
        });
        break;
      case "revealAll":
        this.mw.autoReveal();
        this.setState({ 
          cells: this.mw.fields,
          stats: this.mw.gameStats
        });
        break;
      default:
        console.warn("Unknown revealing button clicked");
    }
  }

  

  render() {
    return (
      <>
      <div className="text-center">
        <h3>Minesweeper</h3>
        <NewGameButtons handleNewGameClick={this.handleNewGameClick}/>
        <br />
        <StatusInfo stats={this.state.stats}/>
        <br />
        <BoardComponent width={this.state.width} height={this.state.height} cells={this.state.cells} onClick={this.handleCellClick}/>                
      </div>
      <div className="text-center">
        <div>
          You can mark obvious mines or reveal all safe fields based on your marks
        </div>
        <br />
        <RevealingButtons handleRevealButtonsClick={this.handleRevealingButtonClick}/>
      </div>
    </>
    )
  }
}

export default MinesweeperComponent;